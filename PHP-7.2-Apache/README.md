# Apache-PHP docker app

# Build and run
```
$ docker build -t my-app-php .
$ docker run -d --name my-running-app -p 8080:80 my-app-php
```

# More info
https://hub.docker.com/_/php

eu.gcr.io/gan-staging-296609/dockerized-app:it-worked
